def sides(x,y,z):
    return set([(x+1,y,z),(x-1,y,z),(x,y+1,z),(x,y-1,z),(x,y,z+1),(x,y,z-1)])

with open("day18/input") as f:
    cs = f.read().splitlines()
cubes = set([tuple(map(int, c.split(','))) for c in cs])

print(sum((s not in cubes) for (x,y,z) in cubes for s in sides(x,y,z)))

min_x = min(c[0] for c in cubes)
min_y = min(c[1] for c in cubes)
min_z = min(c[2] for c in cubes)

max_x = max(c[0] for c in cubes)
max_y = max(c[1] for c in cubes)
max_z = max(c[2] for c in cubes)

seen = set()
todo = [(min_x,min_y,min_z)]
while todo:
    x,y,z = todo.pop()
    for nx, ny, nz in sides(x,y,z):
        if min_x <= nx <= max_x and min_y <= ny <= max_y and min_z <= nz <= max_z:
            todo += [s for s in (sides(x,y,z).difference(cubes, seen))]
    seen.add((x,y,z))

print(sum((s in seen) for (x,y,z) in cubes for s in sides(x,y,z)))