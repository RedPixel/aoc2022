def dec(s):
    d = 0
    for i, c in enumerate(s[::-1]):
        if c == '-':
            d -= 5 ** i
        elif c == '=':
            d -= 2 * (5 ** i)
        else:
            d += int(c) * (5 ** i)
    return d


map = {0: '=', 1: '-', 2: '0', 3: '1', 4: '2'}
def snafu(d):
    if d == 0:
        return ''
    a, b = divmod(d+2, 5)
    return snafu(a) + map[b]


with open('day25/input') as f:
    ls = f.read().splitlines()
print(snafu(sum([dec(x) for x in ls])))
