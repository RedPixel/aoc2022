with open('day1/input', 'r') as f:
    css = f.read().split('\n\n')

sorted_cals = sorted([sum([int(c) for c in cs.split('\n')]) for cs in css])

print( sorted_cals[-1]  )
print( sum(sorted_cals[-3:]) )
