with open('day8/input') as f:
    hs = [[int(d) for d in r] for r in f.read().splitlines()]


def distance(v, l):
    dist = 0
    for h in l:
        dist += 1
        if h >= v:
            return dist
    return dist


max_s = 0
for i in range(len(hs[0])):
    for j in range(len(hs)):
        c = hs[i][j]
        row = hs[i]
        col = [row[j] for row in hs]
        dn = distance(c, col[0:i][::-1])
        dw = distance(c, row[0:j][::-1])
        ds = distance(c, col[i+1:])
        de = distance(c, row[j+1:])
        ss = dn*de*ds*dw
        if(ss > max_s):
            max_s = ss

print(max_s)
