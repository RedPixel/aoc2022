with open('day8/input') as f:
    hs = [[int(d) for d in r] for r in f.read().splitlines()]


s = 0
for i in range(len(hs[0])):
    for j in range(len(hs)):
        c = hs[i][j]
        row = hs[i]
        col = [row[j] for row in hs]
        if all(h < c for h in row[0:j]) or all(h < c for h in row[j+1:]) or all(h < c for h in col[0:i]) or all(h < c for h in col[i+1:]):
            s += 1

print(s)
