from functools import cache
from parse import parse


@cache
def action(time=30, curr='AA', opened=()):
    if time == 0:
        return 0

    best = 0
    for v in tunnels[curr]:
        best = max(best, action(time-1, v, opened))

    if not curr in opened and rates[curr] > 0 and time > 0:
        best = max(best, (time-1) * rates[curr] + action(time-1, curr, opened+(curr,)))
    return best


with open("day16/input") as f:
    ls = f.read().splitlines()

rates = {}
tunnels = {}

for l in ls:
    v, r, t = parse('Valve {} has flow rate={:d}; tunnels {}', l)
    rates[v] = r
    tunnels[v] = t.split(', ')

print(action())
