x = [int(x)*811589153 for x in open('day20/input')]
j = list(range(len(x)))

for _ in range(10):
    for i in range(len(x)):
        c = j.index(i)
        j.pop(c)
        j.insert((c+x[i]) % len(j), i)

z = j.index(x.index(0))
print(sum(x[j[(z+i) % len(j)]] for i in [1000, 2000, 3000]))
