from parse import parse

with open("day15/input") as f:
    ls = f.read().splitlines()
sensors = [parse("Sensor at x={:d}, y={:d}: closest beacon is at x={:d}, y={:d}", l) for l in ls]
ivals = []

for sx, sy, bx, by in sensors:
    d = abs(sx-bx) + abs(sy-by) - abs(sy-2000000)
    if d >= 0:
        ivals.append((sx-d, sx+d))

us = []
for lo, hi in sorted(ivals):
    if us and us[-1][1] >= lo-1:
        us[-1][1] = max(us[-1][1], hi)
    else:
        us.append([lo, hi])

print(sum(hi-lo for lo, hi in us))
