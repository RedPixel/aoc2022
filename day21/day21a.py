def calc(name, ms):
    if isinstance(ms[name], int):
        return ms[name]
    parts = ms[name].split()
    part1 = calc(parts[0], ms)
    part2 = calc(parts[2], ms)

    if parts[1] == "+":
        return part1 + part2
    elif parts[1] == "-":
        return part1 - part2
    elif parts[1] == "*":
        return part1 * part2
    elif parts[1] == "/":
        return part1 / part2

ms = {}
with open("day21/input") as f:
    ls = f.read().splitlines()
for l in ls:
    a, b = l.split(': ')
    if b.isnumeric():
        b = int(b)
    ms[a] = b
result = calc("root", ms)

print(result)
