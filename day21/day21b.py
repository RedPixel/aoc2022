def solve(a,b):
    if not isinstance(a, complex):
        a,b = b,a
    return round((b - a.real) / a.imag)

def calc(name, ms):
    if isinstance(ms[name], int):
        return ms[name]
    if isinstance(ms[name], complex):
        return ms[name]
    
    parts = ms[name].split()
    part1 = calc(parts[0], ms)
    part2 = calc(parts[2], ms)

    if(name == 'root'):
        return solve(part1, part2)

    if parts[1] == "+":
        return part1 + part2
    elif parts[1] == "-":
        return part1 - part2
    elif parts[1] == "*":
        return part1 * part2
    elif parts[1] == "/":
        return part1 / part2

ms = {}
with open("day21/input") as f:
    ls = f.read().splitlines()
for l in ls:
    a, b = l.split(': ')    
    if b.isnumeric():
        b = int(b)
    if a == "humn":
        b = 1j
    ms[a] = b
result = calc("root", ms)

print(result)
