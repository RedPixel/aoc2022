with open('day7/input') as f:
    cs = f.read().splitlines()

sizes = {"/": 0}
current = "/"
stack = []

for c in cs:
    args = c.split(' ')
    if args[0] == '$':
        if args[1] == 'cd':
            if args[2] == '..':
                current = stack.pop()
            else:
                stack.append(current)
                current += args[2] + '/'
    elif args[0] == 'dir':
        sizes[current + args[1] + '/'] = 0
    else:
        sizes[current] += int(args[0])
        for dir in stack:
            sizes[dir] += int(args[0])

print(sum(v for v in sizes.values() if v <= 100_000))
needed = 30000000 - (70000000 - sizes["/"])
print(min(v for v in sizes.values() if v >= needed))