with open('day3/input.txt') as f:
    rs = f.read().split()

print(sum(next(ord(l)-96+58*l.isupper() for l in r[:len(r)//2] if l in r[len(r)//2:]) for r in rs))
print(sum([next(ord(l)-96+58*l.isupper() for l in a if l in b and l in c) for a, b, c in [rs[i:i+3] for i in range(0, len(rs), 3)]]))
