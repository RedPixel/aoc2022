
blocks = (
    ((0, 0), (1, 0), (2, 0), (3, 0)),
    ((1, 0), (0, 1), (1, 1), (2, 1), (1, 2)),
    ((0, 0), (1, 0), (2, 0), (2, 1), (2, 2)),
    ((0, 0), (0, 1), (0, 2), (0, 3)),
    ((0, 0), (1, 0), (0, 1), (1, 1)),
)

widths = (4, 3, 3, 1, 2)
height = (1, 3, 3, 4, 2)
chamber = set([(0, 0), (1, 0), (2, 0), (3, 0), (4, 0), (5, 0), (6, 0)])


def check_x(x, y, dx, b, w):
    if dx == -1 and x == 0:
        return False
    if dx == 1 and x + w == 7:
        return False
    for (sx, sy) in b:
        if (x + sx + dx, y + sy) in chamber:
            return False
    return True


def check_y(x, y, b):
    for (sx, sy) in b:
        if (x + sx, y + sy - 1) in chamber:
            return False
    return True


def place_block(x, y, b):
    for (sx, sy) in b:
        chamber.add((x+sx, y+sy))


def isline():
    for x in range(7):
        if (x, top-1) not in chamber:
            return False
    return True


with open("day17/input") as f:
    ms = f.read()

top = m = 0
for a in range(202+1673):
    m, b, w, h, x, y = m % len(ms), blocks[a % 5], widths[a % 5], height[a % 5], 2, top + 4
    while True:
        dx = -1 if ms[m] == '<' else 1
        m += 1
        if check_x(x, y, dx, b, w):
            x += dx
        if check_y(x, y, b):
            y -= 1
        else:
            place_block(x, y, b)
            top = max(top, y+h-1)  # klopt dit?
            break
print(top)
