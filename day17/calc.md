after 202 blocks (top == 308) every 1735 blocks the top is 2673 higher
(10e11-202)//1735 == 576368875 repeats are 576368875 * 1735 == 999999998125 blocks in repetition
this has height 576368875 * 2673 == 1540634002875
10e11 - 999999998125 == 1875 blocks left
1875-202 == 1673 blocks left on top, which has height(202+1673) - height(202) == 5549 - 2981 == 2876 - 308 == 2568

so h(202) + h(576368875) + h(1673) == 308 + 1540634002875 + 2568 == 1540634005751