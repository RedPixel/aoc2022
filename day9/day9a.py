def sign(x):
    return 0 if x == 0 else -1 if x < 0 else 1


with open('day9/input') as f:
    ms = f.read().splitlines()

dirs = {'U': 1j, 'D': -1j, 'L': -1, 'R': 1}
h = t = 0
seen = set([t])

for move in ms:
    dir, n = move.split(' ')
    for _ in range(int(n)):
        h += dirs[dir]
        d = h-t
        if abs(d) >= 2:
            t += sign(d.real) + 1j * sign(d.imag)
            seen.add(t)

print(len(seen))
