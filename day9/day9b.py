def sign(x):
    return 0 if x == 0 else -1 if x < 0 else 1


with open('day9/input') as f:
    ms = f.read().splitlines()

dirs = {'U': 1j, 'D': -1j, 'L': -1, 'R': 1}
h = [0]*10
seen = set([0])

for move in ms:
    dir, n = move.split(' ')
    for _ in range(int(n)):
        h[0] += dirs[dir]
        for i in range(1, 10):
            d = h[i-1]-h[i]
            if abs(d) >= 2:
                h[i] += sign(d.real) + 1j * sign(d.imag)
                if(i == 9):
                    seen.add(h[9])

print(len(seen))
