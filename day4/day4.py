from parse import parse
with open('day4/input') as f:
    ls = f.read().split()

ps = [parse("{:d}-{:d},{:d}-{:d}", l) for l in ls]
print(sum((a <= c and b >= d) or (c <= a and d >= b) for (a, b, c, d) in ps))
print(sum((b >= c and b <= d) or (d >= a and d <= b) for (a, b, c, d) in ps))
