from collections import deque
from parse import parse

with open('day5/input.txt') as f:
    s, m = f.read().split('\n\n')
s = s.split('\n')
m = m.split('\n')

dqs = [deque() for _ in range(10)]
dq = deque()

for line in s:
    for i in range(9):
        l = line[i*4+1]
        if l.isalpha():
            dqs[i].append(l)

for move in m:
    n, a, b = parse("move {:d} from {:d} to {:d}", move)
    for _ in range(n):
        dq.appendleft(dqs[a-1].popleft())
    for _ in range(n):
        dqs[b-1].appendleft(dq.popleft())

for i in range(9):
    print(dqs[i][0], end='')
