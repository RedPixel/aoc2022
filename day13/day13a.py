def compare(a, b):
    if type(a) is list and type(b) is int:
        b = [b]
    if type(a) is int and type(b) is list:
        a = [a]
    if type(a) is int and type(b) is int:
        if a < b:
            return -1
        elif a == b:
            return 0
        else:
            return 1
    # both are lists
    for x, y in zip(a, b):
        r = compare(x, y)
        if r != 0:
            return r
    if len(a) < len(b):
        return -1
    elif len(a) == len(b):
        return 0
    else:
        return 1


with open("day13/input") as f:
    pairs = f.read().split('\n\n')
    packets = [[eval(packet) for packet in pair.splitlines()]
               for pair in pairs]

print(sum(i for i, (left, right) in enumerate(
    packets, 1) if compare(left, right) == -1))
