def compare(a, b):
    if type(a) is list and type(b) is int:
        b = [b]
    if type(a) is int and type(b) is list:
        a = [a]
    if type(a) is int and type(b) is int:
        if a < b:
            return -1
        elif a == b:
            return 0
        else:
            return 1
    # both are lists
    for x, y in zip(a, b):
        r = compare(x, y)
        if r != 0:
            return r
    if len(a) < len(b):
        return -1
    elif len(a) == len(b):
        return 0
    else:
        return 1


with open("day13/input") as f:
    ls = [eval(l) for l in f.read().replace('\n\n', '\n').split('\n')]
d1 = eval('[[2]]')
a = 1 + sum(compare(l, d1) <= 0 for l in ls)
d2 = eval('[[6]]')
b = 2 + sum(compare(l, d2) <= 0 for l in ls)
print(a, b, a*b)
