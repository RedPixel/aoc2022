from collections import defaultdict, deque


def occupied(e, poss):
    for p in poss:
        if e+p in elves:
            return True
    return False


N, S, W, E = -1j, 1j, -1, 1
nswe = {'N': N, 'S': S, 'W': W, 'E': E}
adj = [N, N+E, N+W, W, E, S, S+E, S+W]
sides = {
    'N': [N, N+E, N+W],
    'S': [S, S+E, S+W],
    'W': [W, N+W, S+W],
    'E': [E, N+E, S+E]
}

with open("day23/input") as file:
    ls = file.read().splitlines()
elves = set((x+y*1j) for y, row in enumerate(ls) for x, c in enumerate(row) if c == "#")

order = deque('NSWE')

# for r in range(10):
x = 1
r = 0
while x != 0:
    r += 1
    next_elves = set()
    proposals = defaultdict(list)

    for e in elves:
        if not occupied(e, adj):
            next_elves.add(e)
        else:
            for o in order:
                if not occupied(e, sides[o]):
                    proposals[e + nswe[o]].append(e)
                    break
            else:
                next_elves.add(e)

    x = 0
    for p in proposals:
        if len(proposals[p]) == 1:
            x += 1
            next_elves.add(p)
        else:
            for o in proposals[p]:
                next_elves.add(o)

    elves = next_elves
    order.append(order.popleft())
print(r)
