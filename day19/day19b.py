from parse import parse
from math import ceil

with open("day19/inputb") as f:
    ms = f.read().splitlines()
p = 1
for m in ms:
    bpidx, oreore, clayore, obsore, obsclay, geoore, geoobs = parse(
        "Blueprint {:d}: Each ore robot costs {:d} ore. Each clay robot costs {:d} ore. Each obsidian robot costs {:d} ore and {:d} clay. Each geode robot costs {:d} ore and {:d} obsidian.", m)

    botcosts = ((oreore, 0, 0, 0),
                (clayore, 0, 0, 0),
                (obsore, obsclay, 0, 0),
                (geoore, 0, geoobs, 0))

    max_geodes = 0
    max_time = 32
    max_bots_needed = (max(oreore, clayore, obsore, geoore),
                       obsclay, geoobs, 24)
    states = [([0, 0, 0, 0], [1, 0, 0, 0], 0)]

    while states:
        resources, bots, time = states.pop()
        geodes = resources[3] + bots[3] * (max_time - time)
        max_geodes = max(max_geodes, geodes)
        time += 1
        if time > max_time:
            continue

        for t in range(4):  # gonna build a robot of type t
            if resources[t] > max_bots_needed[t] + 5: # cheat
            # if bots[t] > max_bots_needed[t]:
                continue
            botcost = botcosts[t]
            bottime = 0
            for r in range(3):
                if resources[r] >= botcost[r]:
                    continue
                if botcost[r] > 0 and bots[r] == 0:
                    bottime = 10000
                    break
                if botcost[r] > 0:
                    bottime = max(bottime, ceil(
                        (botcost[r] - resources[r]) / bots[r]))
            if bottime > (max_time - time):
                continue

            nextresources = resources[:]
            nextbots = bots[:]

            for r in range(4):
                nextresources[r] += bots[r] * (bottime+1) - botcost[r]

            nextbots[t] += 1

            states.append((nextresources, nextbots, time+bottime))
    print(bpidx, max_geodes)
    p *= max_geodes
print(p)
