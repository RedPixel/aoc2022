from parse import parse

with open("day11/input") as f:
    ms = f.read().split('\n\n')

monkeys = []
for m in ms:
    n, items, op, factor, div, tm, fm = parse("""Monkey {:d}:
  Starting items: {}
  Operation: new = old {} {}
  Test: divisible by {:d}
    If true: throw to monkey {:d}
    If false: throw to monkey {:d}""", m)
    items = [int(item) for item in items.split(", ")]
    monkeys.append({'n': n, 'items': items, 'op': op, 'factor': factor,
                   'div': div, 'tm': tm, 'fm': fm, 'inspects': 0})

for round in range(20):
    for m in monkeys:
        for _ in range(len(m['items'])):
            m['inspects'] += 1
            val = m['items'].pop(0)

            if m['factor'] == 'old':
                val *= val
            else:
                if m['op'] == '+':
                    val += int(m['factor'])
                else:
                    val *= int(m['factor'])

            val = val // 3

            if val % m['div'] == 0:
                monkeys[m['tm']]['items'].append(val)
            else:
                monkeys[m['fm']]['items'].append(val)

ans = sorted([m['inspects'] for m in monkeys])
print(ans[-1]*ans[-2])
