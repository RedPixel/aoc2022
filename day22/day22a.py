from functools import reduce
from re import findall


with open("day22/input") as f:
    map,acs = f.read().split('\n\n')
map = map.splitlines()

grid = {i + 1j*j: (c == '.') for i, line in enumerate(map) for j, c in enumerate(line) if c in '.#'}

def move(x, d):
    nx = x + d
    if nx in grid:
        return nx
    else:
        l = 0
        while True:
            prev_coord = x - l*d
            if prev_coord not in grid:
                return prev_coord + d           
            l+=1

pos = (50j,1j)
actions = findall('\d+|R|L', acs)
for a in actions:
    if a in 'RL':
        pos = (pos[0], pos[1] * 1j * (1 - 2 * (a == "R")))
    else:
        pos = (reduce(lambda y, _: move(y, pos[1]) if grid[move(y, pos[1])] else y, range(int(a)), pos[0]), pos[1])

final_coord = 1000*pos[0].real + 4*pos[0].imag + 1004 + [1j, 1, -1j, -1].index(pos[1])
print(int(final_coord))
