from collections import deque


with open("day12/input") as f:
    hs = f.read().splitlines()

dq = deque()
visited = set()
for i in range(len(hs)):
    for j in range(len(hs[i])):
        if hs[i][j] == 'S' or hs[i][j] == 'a':
            dq.append((i, j, 0, 0))
            visited.add((i, j))

while len(dq):
    i, j, dist, curr_height = dq.popleft()
    if hs[i][j] == 'E':
        print(dist)
        exit()
    for next_i, next_j in [(i+1, j), (i-1, j), (i, j+1), (i, j-1)]:
        if (next_i, next_j) in visited or not 0 <= next_i < len(hs) or not 0 <= next_j < len(hs[next_i]): continue
        new_height = ord(hs[next_i][next_j])-ord('a')
        if new_height > curr_height+1: continue
        dq.append((next_i, next_j, dist+1, new_height))
        visited.add((next_i, next_j))
