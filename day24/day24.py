from collections import deque
from functools import cache


def tick(grid):
    new = set()
    for d, (x, y) in grid:
        if d == '^':
            y = y-1 if y > 1 else h-1
        elif d == 'v':
            y = y+1 if y < h-1 else 1
        elif d == '<':
            x = x-1 if x > 1 else w-1
        elif d == '>':
            x = x+1 if x < w-1 else 1
        new.add((d, (x, y)))
    return new


grids = {}


def grid_at(time):
    if time not in grids:
        grids[time] = tick(grid_at(time-1))
    return grids[time]


@cache
def occupied(time):
    return set([g[1] for g in grid_at(time)])


grid_0 = set()
with open('day24/input') as f:
    ls = f.read().splitlines()
for y, l in enumerate(ls):
    for x, c in enumerate(l):
        if c in ['<', '>', '^', 'v']:
            grid_0.add((c, (x, y)))
grids[0] = grid_0
w,h = x,y

seen = set()
start = pos = (1, 0)
target = (w-1, h)
states = deque([(pos, 0)])
runs = 0
while states:
    state = states.popleft()
    if state in seen:
        continue
    seen.add(state)
    pos, step = state

    dirs = ((0, 0), (0, 1), (1, 0), (0, -1), (-1, 0))
    for d in dirs:
        nx, ny = pos[0]+d[0], pos[1]+d[1]
        if (nx, ny) == target:
            if runs == 0:
                print(step+1)
                states = deque([(target, step+1)])
                start = target
                target = (0, 1)
                runs += 1
            elif runs == 1:
                # print(target, step+1)
                states = deque([(target, step+1)])
                start = target
                target = (w-1, h)
                runs += 1
            elif runs == 2:
                print(step+1)
                exit()

        if (0 < nx < w and 0 < ny < h) or (nx, ny) == start:
            if (nx, ny) not in occupied(step+1):
                states.append(((nx, ny), step+1))
