with open("day10/input") as f:
    ls = f.read().splitlines()

xs = [1]
for l in ls:
    x = xs[-1]
    xs.append(x)
    if l.startswith('addx'):
        xs.append(x + int(l[5:]))

print(sum(i*xs[i-1] for i in (20, 60, 100, 140, 180, 220)))

for i, x in enumerate(xs):
    if i % 40 == 0:
        print()
    if -1 <= (i%40)-x <= 1:
        print('#', end='')
    else:
        print(' ', end='')
