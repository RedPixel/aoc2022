with open('day6/input') as f:
    signal = f.read()

print(next(i for i in range(4, len(signal)+1) if len(set(signal[i-4:i])) == 4))
print(next(i for i in range(14, len(signal)+1) if len(set(signal[i-14:i])) == 14))

