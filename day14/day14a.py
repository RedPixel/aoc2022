with open("day14/input") as f:
    ls = f.read().splitlines()

cave = set()
floor = 0
for line in ls:
    cs = [tuple(map(int, p.split(","))) for p in line.split(" -> ")]
    for (x1, y1), (x2, y2) in zip(cs, cs[1:]):
        (x1, x2), (y1, y2) = sorted((x1, x2)), sorted((y1, y2))
        floor = max(floor, y2)
        for cs in range(x1, x2 + 1):
            for y in range(y1, y2 + 1):
                cave.add((cs, y))

n = 0
while y <= floor:
    x, y = (500, 0)
    while y <= floor:
        if (x, y+1) not in cave:
            y += 1
        elif (x-1, y+1) not in cave:
            (x, y) = (x-1, y+1)
        elif (x+1, y+1) not in cave:
            (x, y) = (x+1, y+1)
        else:
            cave.add((x, y))
            n += 1
            break
print(n)
